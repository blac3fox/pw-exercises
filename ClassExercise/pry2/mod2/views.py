from django.shortcuts import render, redirect
from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q


# Create your views here.

from .models import Auto
from .forms import AutoForm


##Modulos necesarios para APIS 
from .serializers import AutoSerialize
from rest_framework import generics

#Vistas de las APIS
class AutoAPICreate(generics.CreateAPIView):
	serializer_class = AutoSerialize

	def perform_create(self , serializer):
		serializer.save()


class AutoAPIList(generics.ListAPIView):
	serializer_class = AutoSerialize

	def get_queryset(self , *args , **kwargs):
		return Auto.objects.all()


# CREATE
def create(request):
	form = AutoForm(request.POST or None)
	if request.user.is_authenticated:
		message = "User Is Logged"
		if form.is_valid():
			instance = form.save(commit=False)
			instance.trainer = request.user
			instance.save()
			return redirect("list")
	else:
		message = "User Must Be Logged"

	context = {
		"form": form,
		"message": message
	}
	return render(request, "auto/create.html", context)

# RETRIEVE
def list(request):
	queryset = Auto.objects.all()
	context = {
		"autos": queryset
	}
	return render(request, "auto/list.html", context)

def detail(request, id):
 	queryset = Auto.objects.get(id=id)	
 	context = {
 		"object": queryset
 	}
 	return render(request, "auto/detail.html", context)


# UPDATE
def update(request, id):
	auto = Auto.objects.get(id=id)
	if  request.method == "GET":
		form = AutoForm(instance=auto )
	else:
		form = AutoForm(request.POST, instance=auto )
		if form.is_valid():
			form.save()
		return redirect("list")
	context = {
		"form": form
	}
	return render(request, "auto/update.html", context)

# DELETE


def delete(request, id):
	auto = Auto.objects.get(id=id)
	if request.method == "POST":
		auto.delete()
		return redirect("list")
	context = {
		"object": auto
	}
	return render(request, "auto/delete.html", context)

#Generic views 
class Create(generic.CreateView):
	template_name = "auto/create2.html"
	model = Auto
	fields = [
		"matricula",
		"marca",
		"año",
		"color",
		"tipo",
		"status",
	]
	success_url = reverse_lazy("list")

class List(generic.ListView):
	template_name = "auto/list2.html"
	queryset = Auto.objects.filter()


	def get_queryset(self, *args, **kwargs):
		qs = Auto.objects.all()
		query = self.request.GET.get('q',None)
		if(query):
			qs = qs.filter(Q(matricula__icontains =  query))
		return qs


	def get_context_data(self, *args, **kwargs):
		context = super(List, self).get_context_data(*args, **kwargs)
		context["message"] = "XD"
		return context

class Detail(generic.DetailView):
	template_name = "auto/detail2.html"
	model = Auto

class Update(generic.UpdateView):
	template_name = "auto/update2.html"
	model = Auto
	fields = [
		"matricula",
		"marca",
		"año",
		"color",
		"tipo",
		"status",
	]
	success_url = reverse_lazy("list")


class Delete(generic.DeleteView):
	template_name = "auto/delete2.html"
	model = Auto
	success_url = reverse_lazy("list")


