from django.db import models
from django.db.models.signals import post_save , pre_save, post_delete , pre_delete
from mod2 import signals
from mod2 import validators

#Manejadores
class AutoQueryset(models.QuerySet):
	def auto_activo(self):
		return self.filter(status = False )
	def auto_azul(self):
		return self.filter(color = "Azul")
	def auto_estandar(self):
		return self.filter(tipo = "Estandar")
	def auto_automatico(self):
		return self.filter(tipo = "Automatico")

# Create your models here.
class Auto(models.Model):
	matricula = models.CharField(max_length = 20, validators = [validators.validation_matricula])
	marca = models.CharField(max_length = 20)
	año = models.IntegerField()
	color = models.CharField(max_length = 20)
	tipo = models.CharField(max_length = 20 , choices = [ ('A','Automatico') , ('E','Estandar') ])
	status = models.BooleanField()
	objects = AutoQueryset.as_manager()

	def __str__(self):
		return self.matricula

post_save.connect(signals.post_save_auto_receiver , sender = Auto)
pre_save.connect(signals.pre_save_auto_receiver , sender = Auto)
pre_delete.connect(signals.pre_delete_auto_receiver , sender = Auto)
post_delete.connect(signals.post_delete_auto_receiver  , sender = Auto)
