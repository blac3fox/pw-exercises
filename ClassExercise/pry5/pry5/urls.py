"""pry5 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.urls import path, include

from main import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.index,    name = "Index"),
    path('mod5/', include('mod5.urls')),
    path('1/', views.temp1, name="Temp1"),
    path('2/', views.temp2, name="Temp2"),
    path('3/', views.temp3, name="Temp3"),
    path('4/', views.temp4, name="Temp4"),
    path('5/', views.temp5, name="Temp5"),
    path('6/', views.temp6, name="Temp6"),
    path('7/', views.temp7, name="Temp7"),
    path('8/', views.temp8, name="Temp8"),
    path('9/', views.temp9, name="Temp9"),
    path('10/', views.temp10, name="Temp10"),
]
