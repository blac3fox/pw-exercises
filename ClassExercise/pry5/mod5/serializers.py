from rest_framework import serializers

from .models import Mascota 


class MascotaSerialize(serializers.ModelSerializer):
	class Meta:
		model = Mascota
		fields = [
			"nombre",
			"edad",
			"especie",
			"sexo",
			"estado",
		]