from django.core.exceptions import ValidationError

def validation_age(value):
	age = value
	if(age <= 0):
		raise ValidationError("Age can not be negative")
	return value

