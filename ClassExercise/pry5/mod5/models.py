from django.db import models
from django.db.models.signals import post_save , pre_save, post_delete , pre_delete
from mod5 import signals
from mod5 import validators

#Manejadores
class MascotaQueryset(models.QuerySet):
	def mascota_macho(self):
		return self.filter(sexo = "M")
	def mascota_hembra(self):
		return self.filter(sexo = "H")
	def mascota_activo(self):
		return self.filter(estado = True)
	def mascota_inactivo(self):
		return self.filter(estado = False)


# Create your models here.
class Mascota(models.Model):
	nombre = models.CharField(max_length = 30)
	edad = models.IntegerField(validators = [validators.validation_age])
	especie = models.CharField(max_length = 30)
	sexo = models.CharField(max_length = 30 , choices = [('M','Macho'),('H','Hembra')])
	estado = models.BooleanField()
	objects = MascotaQueryset.as_manager()

	def __str__(self):
		return self.nombre

post_save.connect(signals.post_save_mascota_receiver , sender = Mascota)
pre_save.connect(signals.pre_save_mascota_receiver , sender = Mascota)
pre_delete.connect(signals.pre_delete_mascota_receiver , sender = Mascota)
post_delete.connect(signals.post_delete_mascota_receiver  , sender = Mascota)
