from django.db import models
from django.db.models.signals import post_save , pre_save, post_delete , pre_delete
from mod3 import signals
from mod3 import validators
#Manejadores
class PeliculaQueryset(models.QuerySet):
	def pelicula_accion(self):
		return self.filter(genero = "Accion" )
	def pelicula_comedia(self):
		return self.filter(genero = "Comedia" )
	def pelicula_drama(self):
		return self.filter(genero = "Drama" )
	def pelicula_terror(self):
		return self.filter(genero = "Terror" )

# Create your models here.
class Pelicula(models.Model):
	titulo = models.CharField(max_length = 50)
	director = models.CharField(max_length = 50)
	año = models.IntegerField()
	genero = models.CharField(max_length = 50 , choices =  [('Accion','Accion') , ('Comedia','Comedia') , ('Drama','Drama') , ('Terror', 'Terror') , ('Otro','Otro')])
	duracion = models.DurationField(validators = [validators.validation_duration])
	sinopsis = models.TextField()
	objects = PeliculaQueryset.as_manager()

	def __str__(self):
		return self.titulo

post_save.connect(signals.post_save_peliculas_receiver , sender = Pelicula)
pre_save.connect(signals.pre_save_peliculas_receiver , sender = Pelicula)
pre_delete.connect(signals.pre_delete_peliculas_receiver , sender = Pelicula)
post_delete.connect(signals.post_delete_peliculas_receiver  , sender = Pelicula)
