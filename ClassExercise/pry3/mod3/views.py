from django.shortcuts import render, redirect
from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q


# Create your views here.

from .models import Pelicula
from .forms import PeliculaForm

##Modulos necesarios para APIS
from .serializers import PeliculaSerialize
from rest_framework import generics

#Vistas de las APIS
class PeliculaAPICreate(generics.CreateAPIView):
	serializer_class = PeliculaSerialize

	def perform_create(self , serializer):
		serializer.save()


class PeliculaAPIList(generics.ListAPIView):
	serializer_class = PeliculaSerialize

	def get_queryset(self , *args , **kwargs):
		return Pelicula.objects.all()


# CREATE
def create(request):
	form = PeliculaForm(request.POST or None)
	if request.user.is_authenticated:
		message = "User Is Logged"
		if form.is_valid():
			instance = form.save(commit=False)
			instance.trainer = request.user
			instance.save()
			return redirect("list")
	else:
		message = "User Must Be Logged"

	context = {
		"form": form,
		"message": message
	}
	return render(request, "peliculas/create.html", context)

# RETRIEVE
def list(request):
	queryset = Pelicula.objects.all()
	context = {
		"peliculass": queryset
	}
	return render(request, "peliculas/list.html", context)

def detail(request, id):
 	queryset = Pelicula.objects.get(id=id)
 	context = {
 		"object": queryset
 	}
 	return render(request, "peliculas/detail.html", context)

# UPDATE
def update(request, id):
	peliculas = Pelicula.objects.get(id=id)
	if  request.method == "GET":
		form = PeliculaForm(instance=peliculas )
	else:
		form = PeliculaForm(request.POST, instance=peliculas )
		if form.is_valid():
			form.save()
		return redirect("list")
	context = {
		"form": form
	}
	return render(request, "peliculas/update.html", context)

# DELETE
def delete(request, id):
	peliculas = Pelicula.objects.get(id=id)
	if request.method == "POST":
		peliculas.delete()
		return redirect("list")
	context = {
		"object": peliculas
	}
	return render(request, "peliculas/delete.html", context)

#Vistas genericas
class List(generic.ListView):
	template_name = "peliculas/list2.html"
	queryset = Pelicula.objects.filter()

	def get_queryset(self, *args, **kwargs):
		qs = Pelicula.objects.all()
		query = self.request.GET.get('q' , None )
		if(query):
			qs = qs.filter(Q( titulo__icontains = query ))
		return qs

class Detail(generic.DetailView):
	template_name = "peliculas/detail2.html"
	model = Pelicula

class Create(generic.CreateView):
	template_name = "peliculas/create2.html"
	model = Pelicula
	fields = [
			"titulo",
			"director",
			"año",
			"genero",
			"duracion",
			"sinopsis",
		]
	success_url = reverse_lazy("list")

class Update(generic.UpdateView):
	template_name = "peliculas/update2.html"
	model = Pelicula
	fields = [
			"titulo",
			"director",
			"año",
			"genero",
			"duracion",
			"sinopsis",
		]
	success_url = reverse_lazy("list")

class Delete(generic.DeleteView):
	template_name = "peliculas/delete2.html"
	model = Pelicula
	success_url = reverse_lazy("list")
