def post_save_receiver(sender, instance, *args, **kwargs):
    print("Object was created!! :D")

def pre_save_receiver(sender, instance, *args, **kwargs):
    print("New Object will be created!! :o")

def post_delete_receiver(sender, instance, *args, **kwargs):
    print("Object was deleted!! :p")

def pre_delete_receiver(sender, instance, *args, **kwargs):
    print("Object will be deleted!! *bigYikes :P *")
