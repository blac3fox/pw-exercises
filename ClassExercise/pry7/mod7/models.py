from django.db import models
from django.db.models.signals import post_save , pre_save, post_delete , pre_delete
from mod7 import signals
from mod7 import validators
#Manejadores
class CelularQueryset(models.QuerySet):
	def celular_android(self):
		return self.filter(so = "Android")
	def celular_ios(self):
		return self.filter(so = "iOS")
	def celular_movistar(self):
		return self.filter(compañias = "Movistar")
	def celular_telcel(self):
		return self.filter(compañias = "Telcel")

# Create your models here.
class Celular(models.Model):
	modelo = models.CharField(max_length = 30)
	marca = models.CharField(max_length = 30 , validators = [validators.validation_brand])
	año = models.IntegerField()
	so = models.CharField(max_length = 30 , choices = [('iOS','iOS'),('Android','Android'),('Otro','Otro')])
	camaras = models.IntegerField()
	compañias = models.CharField(max_length = 30 , choices = [('Telcel','Telcel'),('Movistar','Movistar'),('Otro','Otro')])
	objects = CelularQueryset.as_manager()

	def __str__(self):
		return self.modelo

post_save.connect(signals.post_save_celular_receiver , sender = Celular)
pre_save.connect(signals.pre_save_celular_receiver , sender = Celular)
pre_delete.connect(signals.pre_delete_celular_receiver , sender = Celular)
post_delete.connect(signals.post_delete_celular_receiver  , sender = Celular)
