from django.shortcuts import render, redirect
from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q


# Create your views here.

from .models import Libro
from .forms import LibroForm

##Modulos necesarios para APIS 
from .serializers import LibroSerialize
from rest_framework import generics

#Vistas de las APIS
class LibroAPICreate(generics.CreateAPIView):
	serializer_class = LibroSerialize

	def perform_create(self , serializer):
		serializer.save()


class LibroAPIList(generics.ListAPIView):
	serializer_class = LibroSerialize

	def get_queryset(self , *args , **kwargs):
		return Libro.objects.all()



# CREATE
def create(request):
	form = LibroForm(request.POST or None)
	if request.user.is_authenticated:
		message = "User Is Logged"
		if form.is_valid():
			instance = form.save(commit=False)
			instance.trainer = request.user
			instance.save()
			return redirect("list")
	else:
		message = "User Must Be Logged"

	context = {
		"form": form,
		"message": message
	}
	return render(request, "libro/create.html", context)

# RETRIEVE

def list(request):
	queryset = Libro.objects.all()
	context = {
		"libros": queryset
	}
	return render(request, "libro/list.html", context)

def detail(request, id):
 	queryset = Libro.objects.get(id=id)	
 	context = {
 		"object": queryset
 	}
 	return render(request, "libro/detail.html", context)


# UPDATE
def update(request, id):
	libro = Libro.objects.get(id=id)
	if  request.method == "GET":
		form = LibroForm(instance=libro )
	else:
		form = LibroForm(request.POST, instance=libro )
		if form.is_valid():
			form.save()
		return redirect("list")
	context = {
		"form": form
	}
	return render(request, "libro/update.html", context)

# DELETE
def delete(request, id):
	libro = Libro.objects.get(id=id)
	if request.method == "POST":
		libro.delete()
		return redirect("list")
	context = {
		"object": libro
	}
	return render(request, "libro/delete.html", context)

#Vistas  genericas 
class List(generic.ListView):
	template_name = "libro/list2.html"
	queryset = Libro.objects.filter()


	def get_queryset(self, *args, **kwargs):
		qs = Libro.objects.all()
		query = self.request.GET.get('q' , None)
		if(query):
			qs = qs.filter(Q( titulo__icontains = query ))
		return qs


class Detail(generic.DetailView):
	template_name = "libro/detail2.html"
	model = Libro

class Create(generic.CreateView):
	template_name = "libro/create2.html"
	model = Libro
	fields = [
			"titulo",
			"autor",
			"año",
			"genero",
			"numpaginas",
		]
	success_url = reverse_lazy("list")

class Update(generic.UpdateView):
	template_name = "libro/update2.html"
	model = Libro
	fields = [
			"titulo",
			"autor",
			"año",
			"genero",
			"numpaginas",
		]
	success_url = reverse_lazy("list")

class Delete(generic.DeleteView):
	template_name = "libro/delete2.html"
	model = Libro
	success_url = reverse_lazy("list")

