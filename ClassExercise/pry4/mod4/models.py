from django.db import models
from django.db.models.signals import post_save , pre_save, post_delete , pre_delete
from mod4 import signals
from mod4 import validators
#Manejadores

class LibroQueryset(models.QuerySet):
	def libro_ciencia(self):
		return self.filter(genero = "Ciencia")
	def libro_drama(self):
		return self.filter(genero = "Drama")
	def libro_aventura(self):
		return self.filter(genero = "Aventura")
	def libro_terror(self):
		return self.filter(genero = "Terror")


# Create your models here.
class Libro(models.Model):
	titulo = models.CharField(max_length = 30)
	autor = models.CharField(max_length = 30)
	año = models.IntegerField()
	genero = models.CharField(max_length = 30 , choices = [ ('Ciencia','Ciencia') ,('Drama','Drama') ,('Aventura','Aventura') ,('Terror','Terror') , ('Otro','Otro') ] )
	numpaginas = models.IntegerField(validators = [validators.validation_pages])
	objects = LibroQueryset.as_manager()

	def __str__(self):
		return self.titulo

post_save.connect(signals.post_save_libro_receiver , sender = Libro)
pre_save.connect(signals.pre_save_libro_receiver , sender = Libro)
pre_delete.connect(signals.pre_delete_libro_receiver , sender = Libro)
post_delete.connect(signals.post_delete_libro_receiver  , sender = Libro)
