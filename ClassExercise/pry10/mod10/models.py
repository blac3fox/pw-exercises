from django.db import models
from django.db.models.signals import post_save , pre_save, post_delete , pre_delete
from mod10 import signals
from mod10 import validators 

#Manejadores
class CancionQueryset(models.QuerySet):
	def cancion_rap(self):
		return self.filter(genero = "Rap")
	def cancion_rock(self):
		return self.filter(genero = "Rock")
	def cancion_pop(self):
		return self.filter(genero = "Pop")
	def cancion_electronica(self):
		return self.filter(genero = "Electronica")

# Create your models here.
class Cancion(models.Model):
	titulo = models.CharField(max_length = 30)
	compositor = models.CharField(max_length = 30)
	album = models.CharField(max_length = 30)
	duracion = models.DurationField( validators = [validators.validation_duration])
	genero = models.CharField(max_length = 30, validators = [validators.validation_genero])
	objects = CancionQueryset.as_manager()

	def __str__(self):
		return self.titulo

post_save.connect(signals.post_save_cancion_receiver , sender = Cancion)
pre_save.connect(signals.pre_save_cancion_receiver , sender = Cancion)
pre_delete.connect(signals.pre_delete_cancion_receiver , sender = Cancion)
post_delete.connect(signals.post_delete_cancion_receiver  , sender = Cancion)
