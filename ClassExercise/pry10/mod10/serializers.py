from rest_framework import serializers

from .models import Cancion 

class CancionSerialize(serializers.ModelSerializer):
	class Meta:
		model = Cancion
		fields = [
			"titulo",
			"compositor",
			"album",
			"duracion",
			"genero",
		]