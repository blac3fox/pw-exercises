from rest_framework import serializers

from .models import Pokemon 

class PokemonSerialize(serializers.ModelSerializer):
	class Meta:
		model = Pokemon
		fields = [
			"nombre",
			"tipo",
			"nivel",
			"experiencia",
			"estado",
		]