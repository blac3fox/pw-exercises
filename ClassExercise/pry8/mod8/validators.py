from django.core.exceptions import ValidationError

def validation_type(value):
	pok_type = value 
	if(value.upper() not in ["Fuego","Electrico","Agua","Planta","Roca"]):
		raise ValidationError("Type not valid")
	return value

def validation_level(value):
	level = value 
	if(level < 0 or level >4):
		raise ValidationError("Level is not valid")
	return value

def validation_experience(value):
	exp = value
	if(exp < 0):
		raise ValidationError("Experience can not be negative")
	return value