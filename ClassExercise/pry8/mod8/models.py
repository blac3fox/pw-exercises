from django.db import models
from django.db.models.signals import post_save , pre_save, post_delete , pre_delete
from mod8 import signals
from mod8 import validators 

#Manejadores
class PokemonQueryset(models.QuerySet):
	def pokemon_fuego(self):
		return self.filter(tipo = "Fuego")
	def pokemon_electrico(self):
		return self.filter(tipo = "Electrico")
	def pokemon_activo(self):
		return self.filter(estado = True)
	def pokemon_inactivo(self):
		return self.filter(estado = False)

# Create your models here.
class Pokemon(models.Model):
	nombre = models.CharField(max_length = 30)
	tipo = models.CharField(max_length = 30, validators = [validators.validation_type])
	nivel = models.IntegerField(validators = [validators.validation_level])
	experiencia = models.IntegerField(validators = [validators.validation_experience])
	estado = models.BooleanField()
	objects = PokemonQueryset.as_manager()

	def __str__(self):
		return self.nombre

post_save.connect(signals.post_save_pokemon_receiver , sender = Pokemon)
pre_save.connect(signals.pre_save_pokemon_receiver , sender = Pokemon)
pre_delete.connect(signals.pre_delete_pokemon_receiver , sender = Pokemon)
post_delete.connect(signals.post_delete_pokemon_receiver  , sender = Pokemon)
