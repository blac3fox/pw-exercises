from django.db import models
from django.db.models.signals import post_save , pre_save, post_delete , pre_delete
from mod6 import signals
from mod6 import validators
#Manejadores
class ComputadoraQueryset(models.QuerySet):
	def computadora_dell(self):
		return self.filter(marca = "DELL")
	def computadora_hp(self):
		return self.filter(marca = "HP")
	def computadora_disponible(self):
		return self.filter(estado = True)
	def computadora_no_disponible(self):
		return self.filter(estado = False)


# Create your models here.
class Computadora(models.Model):
	modelo = models.CharField(max_length = 30)
	marca = models.CharField(max_length = 30, validators = [validators.validations_marca])
	procesador = models.CharField(max_length = 30)
	ram = models.IntegerField()
	disco = models.IntegerField()
	estado = models.BooleanField(max_length = 30)
	objects = ComputadoraQueryset.as_manager()

	def __str__(self):
		return self.modelo

post_save.connect(signals.post_save_computadora_receiver , sender = Computadora)
pre_save.connect(signals.pre_save_computadora_receiver , sender = Computadora)
pre_delete.connect(signals.pre_delete_computadora_receiver , sender = Computadora)
post_delete.connect(signals.post_delete_computadora_receiver  , sender = Computadora)
