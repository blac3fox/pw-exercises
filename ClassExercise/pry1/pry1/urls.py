
"""pry1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.contrib.auth.views import logout_then_login

from home import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('mod1/', include('mod1.urls')),
    path('',views.index, name="index_view"),
    path('api-auth', include('rest_framework.urls')),

    path('Temp1/', views.temp1, name="template1"),
    path('Temp2/', views.temp2, name="template2"),
    path('Temp3/', views.temp3, name="template3"),
    path('Temp4/', views.temp4, name="template4"),
    path('Temp5/', views.temp5, name="template5"),
    path('Temp6/', views.temp6, name="template6"),
    path('Temp7/', views.temp7, name="template7"),
    path('Temp8/', views.temp8, name="template8"),
    path('Temp9/', views.temp9, name="template9"),
    path('Temp10/', views.temp10, name="template10"),
]
