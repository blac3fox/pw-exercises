from .models import Estudiante

def messageVer(request):
    context = {
        "msgVersion": "EstudianteApp v0.0.1",}
    return context

def countEstudi(request):
    context = {
        "countEstu": Estudiante.objects.count(),
    }
    return context
