from django.core.validators import ValidationError

def vali_semester(value):
    estudiante_semestre = value
    if estudiante_semestre > 13:
        raise ValidationError("El semestre Maximo es 13!!")
    return value

def vali_edad(value):
    estudiante_edad = value
    if estudiante_edad < 18:
        raise ValidationError("El Estudiante ingresado es menor de edad!!")
    return value
