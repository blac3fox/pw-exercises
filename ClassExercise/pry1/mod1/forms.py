from django import forms

from .models import Estudiante
from django.contrib.auth.forms import UserCreationForm

##Login Forms
class User_Form(UserCreationForm):
    email = forms.EmailField()

class LoginForm(forms.Form):
    username = forms.CharField(max_length=24, widget=forms.TextInput())
    password = forms.CharField(max_length=24, widget=forms.PasswordInput())
##

class EstudianteForms(forms.ModelForm):
    class Meta:
        model = Estudiante
        fields = '__all__'
