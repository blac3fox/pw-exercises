from django.urls import path
from mod1 import views

from django.contrib.auth.views import logout_then_login

urlpatterns=[
    path('list/', views.list, name = "list_view"),
    path('detail/<int:id>/', views.detail, name = "detail_view"),
    path('create/', views.cretate, name = "create_view"),
    path('update/<int:id>/', views.update, name = "update_view"),
    path('delete/<int:id>/', views.delete, name = "delete_view"),
    ##Paths para vistas Genericas
    path('listgen/', views.lists.as_view(), name = "listgen_view"),
    path('detailgen/<int:pk>/', views.details.as_view(), name = "detailgen_view"),
    path('creategen/', views.creates.as_view(), name = "creategen_view"),
    path('updategen/<int:pk>', views.updates.as_view(), name = "updategen_view"),
    path('deletegen/<int:pk>', views.deletes.as_view(), name = "deletegen_view"),
    ##Paths para los registros
    path('home/', views.index, name="homepage_index"),
    path('logout/', logout_then_login, name="logout"),
    path('signup/', views.SignUp.as_view(), name = "signup"),
    ##Paths para APPI's
    path('api_list/',  views.EstudiApiList.as_view(), name = "serializer_view"),
    path('api_create/', views.EstudiApiCreate.as_view(), name = "serializerc_view"),


]
