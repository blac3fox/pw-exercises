from rest_framework import serializers
from .models import Estudiante

class EstudiSerializer(serializers.ModelSerializer):
    class Meta:
        model = Estudiante
        fields = '__all__'
