from django.db import models
from mod1 import validators
from django.db.models.signals import post_save, pre_save, post_delete, pre_delete
from mod1 import signals

#Managers
class Estudiante_QuerySet(models.QuerySet):
    def estudi_bestWaifu(self):
        return self.filter(estudiante_name = "Azul")
    def estudi_semester(self):
        return self.filter(estudi_semester = 7)
    def estudi_edad(self):
        return self.filter(estudi_edad = 21)


class Estudiante(models.Model):
    estudiante_name = models.CharField(max_length=24)
    estudiante_ap = models.CharField(max_length=24)
    estudiante_am = models.CharField(max_length=24)
    estudiante_edad = models.IntegerField(validators = [validators.vali_edad])
    estudiante_semestre = models.IntegerField(validators = [validators.vali_semester])
    email = models.EmailField()
    slug = models.SlugField()
    objects = Estudiante_QuerySet.as_manager()

    def __str__(self):
        return self.estudiante_name

post_save.connect(signals.post_save_receiver, sender = Estudiante)
pre_save.connect(signals.pre_save_receiver, sender = Estudiante)
post_delete.connect(signals.post_delete_receiver, sender = Estudiante)
pre_delete.connect(signals.pre_delete_receiver, sender = Estudiante)
