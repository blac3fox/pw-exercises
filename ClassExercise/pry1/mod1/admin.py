from django.contrib import admin

# Register your models here.
from .models import  Estudiante


@admin.register(Estudiante)

class AdminEstudiante(admin.ModelAdmin):
    list_display = [
        "estudiante_name",
        "estudiante_ap",
        "estudiante_am",
    ]