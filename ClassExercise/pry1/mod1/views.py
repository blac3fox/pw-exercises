from django.shortcuts import render, redirect
from .models import Estudiante
from .forms import EstudianteForms, LoginForm, User_Form
##Librerias para vistas Genericas
from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q
##Librerias para el Login
from django.contrib.auth import authenticate, login
##Serializers
from .serializar import EstudiSerializer
from rest_framework import generics

##
# Create your views here.

##ViewSerializer
class EstudiApiList(generics.ListAPIView):
    serializer_class = EstudiSerializer

    def get_queryset(self, *args, **kwargs):
        return Estudiante.objects.all()
##
##CreateSerializer
class EstudiApiCreate(generics.CreateAPIView):
    serializer_class = EstudiSerializer

    def perform_create(self, serializer):
        serializer.save()
##
##Registro
class SignUp(generic.FormView):
    template_name = "mod1/signup.html"
    form_class = User_Form
    success_url = reverse_lazy('login')

    def form_valid(self , form):
        user = form.save()
        return super(SignUp, self).form_valid(form)

def index(request):
	form = LoginForm(request.POST or None )
	if(request.method == "POST"):
		if(form.is_valid()):
			username = request.POST['username']
			password = request.POST['password']
			user = authenticate(username = username , password = password)
			if user is not None:
				if user.is_active:
					login(request , user )
					message = 'user logged'
				else:
					message = 'user is not active'
			else:
				message = 'user does not exist'

	context = {
		'form': form,
	}
	return render(request, "mod1/index.html", context)

##
##Listado
def list(request):
    queryset = Estudiante.objects.all()
    context ={
        "lista": queryset
    }
    return render(request, "mod1/list.html", context)
##

##Listado con Generic
class lists(generic.ListView):
    template_name = "mod1/listgen.html"
    queryset = Estudiante.objects.filter()

    def get_queryset(self, *arg, **kwargs):
        qs = Estudiante.objects.all()
        query = self.request.GET.get("q", None)
        if query is not None:
            qs = qs.filter(Q(estudiante_name__icontains=query))
        return qs
##

##Detalles
def detail(request, id):
    queryset = Estudiante.objects.get(id = id)
    context =  {
        "detail" : queryset
    }
    return render(request, "mod1/detail.html", context)
##

##Detalles con Generic
class details(generic.DetailView):
    template_name = "mod1/detailgen.html"
    model = Estudiante

##

##Creacion
def cretate(request):
    form = EstudianteForms(request.POST or None)
    if(request.user.is_authenticated):
        if form.is_valid():
            instance = form.save(commit=False)
            instance.trainer = request.user
            instance.save()
            return redirect("list_view")

    context = {
        "form": form,
    }
    return render(request, "mod1/create.html",context)
##

##Creacion con generic
class creates(generic.CreateView):
    template_name = "mod1/creategen.html"
    model = Estudiante
    fields = '__all__'
    success_url = reverse_lazy('listgen_view')

##

##Actualizacion
def update(request, id):
    queryset = Estudiante.objects.get(id = id)
    if(request.method == "GET"):
        form = EstudianteForms(instance = queryset)
    else:
        form = EstudianteForms(request.POST, instance = queryset)
        if(request.user.is_authenticated):
            if form.is_valid():
                form.save()
        return redirect("list_view")
    context = {
        "form": form,
    }
    return render(request, "mod1/update.html", context)
##

##Actualizacion con generic
class updates(generic.UpdateView):
    template_name = "mod1/updategen.html"
    model = Estudiante
    fields = '__all__'
    success_url = reverse_lazy('listgen_view')
##

##Borrado
def delete(request, id):
    queryset = Estudiante.objects.get(id = id)
    if(request.method == "POST"):
        queryset.delete()
        return redirect("list_view")
    context = {
        "obj": queryset,
    }
    return render(request, "mod1/delete.html", context)
##

##Borrado Gerneric
class deletes(generic.DeleteView):
    template_name = "mod1/deletegen.html"
    model = Estudiante
    success_url = reverse_lazy('listgen_view')

##
