from django.shortcuts import render, redirect
from .forms import LoginForm, User_Form
##Librerias para vistas Genericas
from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q
##Librerias para el Login
from django.contrib.auth import authenticate, login

# Create your views here.

##Registro
class SignUp(generic.FormView):
    template_name = "home/signup.html"
    form_class = User_Form
    success_url = reverse_lazy('login')

    def form_valid(self , form):
        user = form.save()
        return super(SignUp, self).form_valid(form)

def index(request):
	form = LoginForm(request.POST or None )
	if(request.method == "POST"):
		if(form.is_valid()):
			username = request.POST['username']
			password = request.POST['password']
			user = authenticate(username = username , password = password)
			if user is not None:
				if user.is_active:
					login(request , user )
					message = 'user logged'
				else:
					message = 'user is not active'
			else:
				message = 'user does not exist'

	context = {
		'form': form,
	}
	return render(request, "home/index.html", context)


##-------------------------------------------------##

def temp1(request):
    context = {}
    return render(request, "home/p.html", context)

def temp2(request):
    context = {}
    return render(request, "home/p3.html", context)

def temp3(request):
    context = {}
    return render(request, "home/p1.html", context)

def temp4(request):
    context = {}
    return render(request, "home/p2.html", context)

def temp5(request):
    context = {}
    return render(request, "home/p4.html", context)

def temp6(request):
    context = {}
    return render(request, "home/p5.html", context)

def temp7(request):
    context = {}
    return render(request, "home/p6.html", context)

def temp8(request):
    context = {}
    return render(request, "home/p7.html", context)

def temp9(request):
    context = {}
    return render(request, "home/p8.html", context)

def temp10(request):
    context = {}
    return render(request, "home/p9.html", context)

##-------------------------------------------------##
