from django import forms
from django.contrib.auth.forms import UserCreationForm

##Login Forms
class User_Form(UserCreationForm):
    email = forms.EmailField()

class LoginForm(forms.Form):
    username = forms.CharField(max_length=24, widget=forms.TextInput())
    password = forms.CharField(max_length=24, widget=forms.PasswordInput())
##
