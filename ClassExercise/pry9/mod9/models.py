from django.db import models
from django.db.models.signals import post_save , pre_save, post_delete , pre_delete
from mod9 import signals
from mod9 import validators

#Manejadores
class AnimalQueryset(models.QuerySet):
	def animal_acuatico(self):
		return self.filter(tipo = "Acuatico")
	def animal_terrestre(self):
		return self.filter(tipo = "Terrestre")
	def animal_voldaor(self):
		return self.filter(tipo = "Volador")
	def animal_extinto(self):
		return self.filter(extinto = True)

# Create your models here.
class Animal(models.Model):
	nombre = models.CharField(max_length = 30)
	tipo = models.CharField(max_length = 30, choices = [('Acuatico','Acuatico'),('Terrestre','Terrestre'),('Volador','Volador'),('Otro','Otro')] )
	ecosistemas = models.CharField(max_length = 30 , validators = [validators.validation_ecosistema] )
	alimento = models.CharField(max_length = 30, validators = [validators.validation_alimento])
	extinto = models.BooleanField()
	objects = AnimalQueryset.as_manager()

	def __str__(self):
		return self.nombre

post_save.connect(signals.post_save_animal_receiver , sender = Animal)
pre_save.connect(signals.pre_save_animal_receiver , sender = Animal)
pre_delete.connect(signals.pre_delete_animal_receiver , sender = Animal)
post_delete.connect(signals.post_delete_animal_receiver  , sender = Animal)
