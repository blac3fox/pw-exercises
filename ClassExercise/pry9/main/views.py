from django.shortcuts import render

# Create your views here.
def index(request):
    context = {}
    return render(request,"main/index.html", context)
    
def temp1(request):
    context = {}
    return render(request, "main/tem1.html", context)

def temp2(request):
    context = {}
    return render(request, "main/tem2.html", context)

def temp3(request):
    context = {}
    return render(request, "main/tem3.html", context)

def temp4(request):
    context = {}
    return render(request, "main/tem4.html", context)

def temp5(request):
    context = {}
    return render(request, "main/tem5.html", context)

def temp6(request):
    context = {}
    return render(request, "main/tem6.html", context)

def temp7(request):
    context = {}
    return render(request, "main/tem7.html", context)

def temp8(request):
    context = {}
    return render(request, "main/tem8.html", context)

def temp9(request):
    context = {}
    return render(request, "main/tem9.html", context)

def temp10(request):
    context = {}
    return render(request, "main/tem10.html", context)
